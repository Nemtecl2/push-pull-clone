import { browser, element, by } from 'protractor';

/* tslint:disable */
export class PushPullClonePage {
  navigateTo(route: string) {
    return browser.get(route);
  }
}
