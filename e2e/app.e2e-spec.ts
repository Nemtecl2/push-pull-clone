import { PushPullClonePage } from './app.po';
import { browser, element, by } from 'protractor';

describe('push-pull-clone App', () => {
  let page: PushPullClonePage;

  beforeEach(() => {
    page = new PushPullClonePage();
  });

  it('should display message saying App works !', () => {
    page.navigateTo('/');
    expect(element(by.css('app-home h1')).getText()).toMatch('App works !');
  });
});
