import { Component, OnInit, OnDestroy } from '@angular/core';
import { GitService } from '../../providers/git.service';
import { Subscription } from 'rxjs';
import { ElectronService } from '../../providers/electron.service';

@Component({
  selector: 'app-parameters-manager',
  templateUrl: './parameters-manager.component.html',
  styleUrls: ['./parameters-manager.component.scss']
})
export class ParametersManagerComponent implements OnInit, OnDestroy {
  path: any;
  pathSubscription: Subscription;

  constructor(private electronService: ElectronService,
    private gitService: GitService) {
      this.pathSubscription = this.gitService.pathSubject.subscribe(
        (path: any) => {
          this.path = path;
        }
      );
      this.gitService.emitPathSubject();
    }

  ngOnInit() {
  }

  changePath() {
    const newPath = this.electronService.remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    });
    if (newPath !== undefined) {
      this.gitService.setPath(newPath[0]);
    }
  }

  ngOnDestroy() {
    this.pathSubscription.unsubscribe();
  }
}
