import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GitService } from '../../providers/git.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-log-manager',
  templateUrl: './log-manager.component.html',
  styleUrls: ['./log-manager.component.scss']
})
export class LogManagerComponent implements OnInit, OnDestroy {
  path: any;
  pathSubscription: Subscription;
  graph: String;

  constructor(private gitService: GitService,
    private toastr: ToastrService) {
    this.pathSubscription = this.gitService.pathSubject.subscribe(
      (path: any) => {
        this.path = path;
      }
    );
    this.gitService.emitPathSubject();
  }

  ngOnInit() {
  }

  async getLog() {
    this.graph = (await this.gitService.log());
    if (this.graph === '') {
      this.toastr.error('Ce repertoire n\'est pas un repertoire git', 'Erreur', {
        onActivateTick: true
      });
    } else {
      this.graph = this.graph.replace(/\n/g, '<br />');
    }
  }

  ngOnDestroy() {
    this.pathSubscription.unsubscribe();
  }
}
