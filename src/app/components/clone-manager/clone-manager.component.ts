import { Component, OnInit, OnDestroy } from '@angular/core';
import { GitService } from '../../providers/git.service';
import { ElectronService } from '../../providers/electron.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-clone-manager',
  templateUrl: './clone-manager.component.html',
  styleUrls: ['./clone-manager.component.scss']
})
export class CloneManagerComponent implements OnInit, OnDestroy {
  path: any;
  pathSubscription: Subscription;
  destination: string;

  constructor(private gitService: GitService,
    private electronService: ElectronService, private toastr: ToastrService) {
    this.pathSubscription = this.gitService.pathSubject.subscribe(
      (path: any) => {
        this.path = path;
      }
    );
    this.gitService.emitPathSubject();
  }

  ngOnInit() {
  }

  browse() {
    const newPath = this.electronService.remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    });
    if (newPath !== undefined) {
      this.destination = newPath[0];
    }
  }

  onSubmit(form: NgForm) {
    if (this.electronService.fs.existsSync(this.destination)) {
      this.gitService.clone(form.value['username'], form.value['password'], form.value['url'], this.destination);
    } else {
      this.toastr.error('Le repertoire indiqué n\'existe pas', 'Erreur', {
        onActivateTick: true
      });
    }
  }

  ngOnDestroy() {
    this.pathSubscription.unsubscribe();
  }
}
