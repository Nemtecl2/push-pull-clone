import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GitService } from '../../providers/git.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sync-manager',
  templateUrl: './sync-manager.component.html',
  styleUrls: ['./sync-manager.component.scss']
})

export class SyncManagerComponent implements OnInit, OnDestroy {
  action: string;
  path: any;
  pathSubscription: Subscription;

  constructor(private gitService: GitService) {
    this.pathSubscription = this.gitService.pathSubject.subscribe(
      (path: any) => {
        this.path = path;
      }
    );
    this.gitService.emitPathSubject();
  }

  ngOnInit() {
    this.action = 'Pull';
  }

  onSubmit(form: NgForm) {
    if (this.action === 'Pull') {
      this.gitService.pull(form.value['username'], form.value['password']);
    } else if (this.action === 'Push') {
      this.gitService.push(form.value['username'], form.value['password']);
    }
  }

  ngOnDestroy() {
    this.pathSubscription.unsubscribe();
  }
}
