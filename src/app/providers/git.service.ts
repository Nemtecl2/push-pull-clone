import { Injectable } from '@angular/core';
import * as gitPromise from 'simple-git/promise';
import * as simpleGit from 'simple-git';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { ElectronService } from './electron.service';
import * as  GitUrlParse from 'git-url-parse';

@Injectable()
export class GitService {
  path: any;
  pathSubject: Subject<any>;
  gitP: any;
  git: any;

  constructor(private toastr: ToastrService,
    private electronService: ElectronService) {
    this.gitP = gitPromise();
    this.git = simpleGit();
    this.pathSubject = new Subject<any>();
    this.path = this.electronService.process.cwd();
  }

  emitPathSubject() {
    this.pathSubject.next(this.path);
  }

  /**
   * Fonction permettant de tester si currentPath est un repo git
   * @param currentPath le chemin à tester
   */
  isRepo(currentPath: string) {
    return gitPromise(currentPath).checkIsRepo();
  }

  /**
   * Fonction permettant d'afficher le graphe
   */
  async log() {
    if (await this.isRepo(this.electronService.process.cwd())) {
      return this.gitP.raw(
        [
          'log',
          '--oneline',
          '--decorate',
          '--graph',
          '--all'
        ]);
    } else {
      return '';
    }
  }

  /**
   * Fonction permettant de changer le chemin courant
   * @param newPath le nouveau chemin
   */
  async setPath(newPath) {
    if (await this.isRepo(newPath)) {
      this.path = newPath;
      this.electronService.process.chdir(this.path);
      this.git.cwd(this.path);
      this.gitP.cwd(this.path);
      this.emitPathSubject();
      this.toastr.success('Chemin courant changé', 'Succès', {
        onActivateTick: true
      });
    } else {
      this.toastr.error('Ce repertoire n\'est pas un repertoire git', 'Erreur', {
        onActivateTick: true
      });
    }
  }

  /**
   * Fonction permettant de clone un repertoire git (seulement en HTTPS pour le moment)
   * @param username le nom d'utilisateur
   * @param password le mot de passe
   * @param url l'url du repo distant
   * @param destination la destination du clonage sur l'ordinateur
   */
  clone(username: string, password: string, url: string, destination: string) {
    const GIT_URL_PARSE = GitUrlParse(url);
    // Si c'est en SSH, on ne gère pas
    if (GIT_URL_PARSE.protocol === 'ssh') {
      this.toastr.error('Cette PoC ne gère pas (encore) le SSH', 'Erreur', {
        onActivateTick: true
      });
    } else { // Sinon on est en HTTPS
      // Chaîne de la remote pour la connexion en HTTPS
      const remote = `https://${username}:${password}@${GIT_URL_PARSE.resource}${GIT_URL_PARSE.pathname}`;
      // Clone
      gitPromise(destination)
        .silent(true)
        .clone(remote, null)
        .then(() => { // Cas de succès
          const pathToRepo = this.electronService.path.join(destination, GIT_URL_PARSE.name);
          // On cache l'username et le password dans l'url
          gitPromise(pathToRepo)
            .raw(['remote', 'set-url', 'origin', url])
            .then(() => { // Cas de succès
              // Le chemin est celui du repo fraîchement cloné
              this.setPath(pathToRepo);
              this.toastr.success('Le repertoire a été cloné', 'Succès', {
                onActivateTick: true
              });
            })
            .catch((err) => { // Cas d'erreur
              this.toastr.error('Problème lors du clone', 'Erreur', {
                onActivateTick: true
              });
            });
        })
        .catch((err) => { // Cas d'erreur
          this.toastr.error('Problème lors du clone', 'Erreur', {
            onActivateTick: true
          });
        });
    }
  }

  /**
   * Méthode permettant de pull (ne gère pas le SSH et possible qu'avec origin)
   * @param username le nom d'utilisateur
   * @param password le mot de passe
   */
  pull(username: String, password: String) {
    // On vérifie que le chemin courant est un repertoire git
    if (this.isRepo(this.electronService.process.cwd())) {
      // On recupère l'URL d'origin
      this.gitP
        .listRemote(['--get-url', 'origin'])
        .then((data) => {
          data = data.replace('\n', '');
          if (data === 'origin') { // Cas où il n'y a pas de repo distant, erreur
            this.toastr.error('Aucun remote associé à ce projet', 'Erreur', {
              onActivateTick: true
            });
          } else { // Cas où il y a un repo distant
            const GIT_URL_PARSE = GitUrlParse(data);
            // Si c'est en SSH, on ne gère pas
            if (GIT_URL_PARSE.protocol === 'ssh') {
              this.toastr.error('Cette PoC ne gère pas (encore) le SSH', 'Erreur', {
                onActivateTick: true
              });
            } else { // Sinon on est en HTTPS
              // Chaîne de la remote pour la connexion en HTTPS
              const remote = `https://${username}:${password}@${GIT_URL_PARSE.resource}${GIT_URL_PARSE.pathname}`;
              // Pull
              this.gitP.silent(true)
                .pull(remote, 'master')
                .then((update) => {
                  const MSG = update.summary.changes === 0 ? 'Déjà à jour' : 'Pull réussi : ' + update.summary.changes + ' changement';
                  this.toastr.success(MSG, 'Succès', {
                    onActivateTick: true
                  });
                })
                .catch((err) => {
                  console.log(err);
                  this.toastr.error('Problème lors du pull', 'Erreur', {
                    onActivateTick: true
                  });
                });
            }
          }
        })
        .catch((err) => {
          this.toastr.error('Problème lors du pull', 'Erreur', {
            onActivateTick: true
          });
        });
    } else { // Si ce n'eest pas le cas, erreur
      this.toastr.error('Ce repertoire n\'est pas un repertoire git', 'Erreur', {
        onActivateTick: true
      });
    }
  }

  /**
   * Méthode permettant de push (ne gère pas le SSH et possible qu'avec origin)
   * @param username le nom d'utilisateur
   * @param password le mot de passe
   */
  push(username: String, password: String) {
    // On vérifie que le chemin courant est un repertoire git
    if (this.isRepo(this.electronService.process.cwd())) {
      // On recupère l'URL d'origin
      this.gitP
        .listRemote(['--get-url', 'origin'])
        .then((data) => {
          data = data.replace('\n', '');
          if (data === 'origin') { // Cas où il n'y a pas de repo distant, erreur
            this.toastr.error('Aucun remote associé à ce projet', 'Erreur', {
              onActivateTick: true
            });
          } else { // Cas où il y a un repo distant
            const GIT_URL_PARSE = GitUrlParse(data);
            // Si c'est en SSH, on ne gère pas
            if (GIT_URL_PARSE.protocol === 'ssh') {
              this.toastr.error('Cette PoC ne gère pas (encore) le SSH', 'Erreur', {
                onActivateTick: true
              });
            } else { // Sinon on est en HTTPS
              // Chaîne de la remote pour la connexion en HTTPS
              const remote = `https://${username}:${password}@${GIT_URL_PARSE.resource}${GIT_URL_PARSE.pathname}`;
              // Push
              this.gitP
                .push(remote, 'master', [])
                .then(() => {
                  this.toastr.success('Le push a été effectué', 'Succès', {
                    onActivateTick: true
                  });
                })
                .catch((err) => {
                  this.toastr.error('Problème lors du push', 'Erreur', {
                    onActivateTick: true
                  });
                });
            }
          }
        })
        .catch((err) => {
          this.toastr.error('Problème lors du pull', 'Erreur', {
            onActivateTick: true
          });
        });
    } else { // Si ce n'eest pas le cas, erreur
      this.toastr.error('Ce repertoire n\'est pas un repertoire git', 'Erreur', {
        onActivateTick: true
      });
    }
  }
}
